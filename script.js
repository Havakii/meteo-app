var degToCard = function(deg){
    if (deg>11.25 && deg<33.75){
      return "NNE";
    }else if (deg>33.75 && deg<56.25){
      return "ENE";
    }else if (deg>56.25 && deg<78.75){
      return "E";
    }else if (deg>78.75 && deg<101.25){
      return "ESE";
    }else if (deg>101.25 && deg<123.75){
      return "ESE";
    }else if (deg>123.75 && deg<146.25){
      return "SE";
    }else if (deg>146.25 && deg<168.75){
      return "SSE";
    }else if (deg>168.75 && deg<191.25){
      return "S";
    }else if (deg>191.25 && deg<213.75){
      return "SSW";
    }else if (deg>213.75 && deg<236.25){
      return "SW";
    }else if (deg>236.25 && deg<258.75){
      return "WSW";
    }else if (deg>258.75 && deg<281.25){
      return "W";
    }else if (deg>281.25 && deg<303.75){
      return "WNW";
    }else if (deg>303.75 && deg<326.25){
      return "NW";
    }else if (deg>326.25 && deg<348.75){
      return "NNW";
    }else{
      return "N"; 
    }
  }

const imgIcone = document.querySelector('#weatherIcon');

let apiCall = function (city) {

    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&lang=fr&appid=429a04c97630f5f70975c44939a7d2a5`;

    fetch(url).then((response) =>
        response.json().then((data) => {

            console.log(data);
            document.querySelector('#city').innerHTML = data.name;
            document.querySelector('#temp').innerHTML = Math.round(data.main.temp) + '°';
            document.querySelector('#feelTemp').innerHTML = 'Ressenti: ' + Math.round(data.main.feels_like) + '°';
            document.querySelector('#maxTemp').innerHTML = 'Max. ' + Math.round(data.main.temp_max) + '°';
            document.querySelector('#minTemp').innerHTML = 'Min. ' + Math.round(data.main.temp_min) + '°';
            document.querySelector('#humidity').innerHTML = data.main.humidity + '%';
            document.querySelector('#wind').innerHTML = 'Vitesse: ' + Math.round(data.wind.speed) + 'km/h';
            document.querySelector('#windDir').innerHTML ='Direction: ' + degToCard(data.wind.deg);
            document.querySelector('#weather').innerHTML = data.weather[0].main;

            let currentTime = new Date().getHours();

            if(currentTime >= 6 && currentTime< 21) {
                imgIcone.src = `assets/day-svg/${data.weather[0].icon}.svg`
            } else  {
                imgIcone.src = `assets/night-svg/${data.weather[0].icon}.svg`
            };

            
            let weather = document.querySelector('#weather').innerHTML;

            if(weather == 'Clear') {
                document.body.style.background = "url(/assets/background/clear.jpg)";
            }else if (weather == 'Clouds'){
                document.body.style.background = "url(/assets/background/cloudy.jpg)";
            }else if (weather == 'Rain' || weather == 'Drizzle'){
                document.body.style.background = "url(/assets/background/rain.jpg)";
            }else if (weather == 'Snow'){
                document.body.style.background = "url(/assets/background/snow.jpg)";
            }else if (weather == 'Thunderstorm'){
                document.body.style.background = "url(/assets/background/thunder.jpg)";
            }else{
                document.body.style.background = "#ccc";
            };

        })
    );
}

document.querySelector('form').addEventListener('submit', function (e) {
    e.preventDefault();
    let ville = document.querySelector('#inputCity').value;

    apiCall(ville);
});

apiCall('Toulouse');

//Geolocalisation

if (navigator.geolocation) { 
    navigator.geolocation.getCurrentPosition(function(position){
      console.log(position);
    });   
}
